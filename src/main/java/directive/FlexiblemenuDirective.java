package directive;

import com.jfinal.aop.Inject;
import com.jfinal.core.Controller;
import com.jfinal.core.JFinal;
import com.jfinal.plugin.activerecord.Page;
import com.jfinal.template.Env;
import com.jfinal.template.io.Writer;
import com.jfinal.template.stat.Scope;
import io.jboot.web.controller.JbootControllerContext;
import io.jboot.web.directive.annotation.JFinalDirective;
import io.jboot.web.directive.base.JbootDirectiveBase;
import io.jboot.web.directive.base.PaginateDirectiveBase;
import io.jpress.JPressOptions;
import io.jpress.commons.directive.DirectveKit;
import io.jpress.model.Menu;
import io.jpress.service.MenuService;

import javax.servlet.http.HttpServletRequest;


@JFinalDirective("flexiblemenu")
public class FlexiblemenuDirective extends JbootDirectiveBase {

    @Inject
    MenuService menuService;

    @Override
    public void onRender(Env env, Scope scope, Writer writer) {
        String menuid = getPara("menuid", scope);
        Menu menu = menuService.findById(menuid);
        scope.setLocal("menu", menu);
        renderBody(env, scope, writer);
    }

    @Override
    public boolean hasEnd() {
        return true;
    }
}
